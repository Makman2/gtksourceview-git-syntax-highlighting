# gtksourceview-git-syntax-highlighting

Pimp up your git experience! Additional syntax highlighting for the GtkSourceView v2 or above for
git tooling. Refine your daily workflow for your favorite text editors using GtkSourceView like
gedit.

> Disclaimer:
>
> Only works if you have configured gedit or another text editor using GtkSourceView
> to be your `core.editor`.
>
> If you haven't yet, give it a try!
>
> ```bash
> git config --global core.editor "gedit -s"
> ```

## Install

1. Copy the language-specification files in `language-specs` into your
   `/usr/share/gtksourceview-*/language-specs` folder. If you have more than one version
   of GtkSourceView available, copy those files to all of them.
2. Depending on your installed styles, copy the `<style>`-tags out of the style-schemes inside
   the `styles` folder and add them to their respective installed counterparts at
   `/usr/share/gtksourceview-*/styles`.

## Available Language Definitions

- `git commit`

  Highlighting for commit messages. Also supports verbose commits and highlights
  issue references like they occur at GitHub or GitLab.

- `git rebase --interactive`

  Highlight interactive rebase scripts. Different colors for different commands.
