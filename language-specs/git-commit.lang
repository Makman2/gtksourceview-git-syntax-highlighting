<?xml version="1.0" encoding="UTF-8"?>
<language id="git-commit" name="Git Commit" version="2.0" _section="Source">
  <metadata>
    <property name="globs">COMMIT_EDITMSG</property>
  </metadata>
  <styles>
    <style id="comment" name="Comment" map-to="def:comment"/>
    <style id="brief" name="Brief"/>
    <style id="issue-ref" name="Issue Reference"/>
  </styles>

  <definitions>
    <context id="git-commit">
      <include>
        <define-regex id="short-issue-ref">(?:!|#)\d+</define-regex>
        <define-regex id="long-issue-ref">[^\s]+\%{short-issue-ref}</define-regex>
        <define-regex id="full-issue-link">(?:http(s)?:\/\/)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&amp;\/=]*)</define-regex>
        <define-regex id="issue-ref">\%{short-issue-ref}|\%{long-issue-ref}|\%{full-issue-link}</define-regex>

        <context id="verbose-diff" style-ref="comment">
          <start># ------------------------ &gt;8 ------------------------</start>
          <include>
            <context ref="diff:diff" original="true"/>
          </include>
        </context>

        <context id="comment" style-ref="comment" end-at-line-end="true">
          <start>^[ \t]*#</start>
        </context>

        <context id="brief" style-ref="brief" first-line-only="true" end-at-line-end="true">
          <start>^</start>
        </context>

        <context id="issue-ref" style-ref="issue-ref">
          <!-- Regex from https://docs.gitlab.com/ee/user/project/issues/automatic_issue_closing.html -->
          <match>((?:[Cc]los(?:e[sd]?|ing)|[Ff]ix(?:e[sd]|ing)?|[Rr]esolv(?:e[sd]?|ing)|[Ii]mplement(?:s|ed|ing)?)(:?) +(?:(?:issues? +)?\%{issue-ref}(?:(?:, *| +and +)?)|([A-Z][A-Z0-9_]+-\d+))+)</match>
        </context>

      </include>
    </context>
  </definitions>
</language>
